# Verde 

[Verde] is a dynamically built static html website utilizing Kentico Cloud, ruby, middleman & [Heroku].


## Running Locally

```
$ bundle install
$ middleman deliver
$ middleman build
$ heroku local web
```

View at http://localhost:500


## Deploying

To deploy to heroku (optionally specify a feature branch to deploy a branch)

```
$ git push heroku <feature-branch>:master
```


## Auto Build

In the Scheduler in [Heroku], the bin/rebuild.rb is executed every hour. This script
grabs an access token from Bitbucket (since it's a private repo) and then posts the
tar of the repo to [Heroku] which is essentially the same as 'git push heroku master'.

[Heroku] recommends the bin/ directory for non-Rails apps and to put a rake file
in the lib/tasks/ directory for Rails apps.


[Verde]: https://cryptic-temple-55866.herokuapp.com
[Heroku]: http://heroku.com
