// JavaScript Document

;(function($) {

$(document).ready(function() {

	var overallHeight = $(window).height();
	var overallWidth = $(window).width();
	var headerHeight = $('header').outerHeight();

	$(window).resize(function() {
		overallHeight = $(window).height();
		overallWidth = $(window).width();
		topBarHeight = $('.topBars').outerHeight();
		headerHeight = $('header').outerHeight();

		reorderHp();
	});

	// STICKY HEADER ON SCROLL
    var topBarHeight = $('.topBars').outerHeight();
	$(window).scroll(function() {
	    var scroll = $(window).scrollTop();

	    if (scroll >= topBarHeight && overallWidth >= 768) {
	        $('header').addClass('headerScrolled');
	        $('.topBars').css({marginBottom: headerHeight});
	    } else {
	        $('header').removeClass('headerScrolled');
	        $('.topBars').css({marginBottom: 0});
	    }

	    headerHeight = $('header').outerHeight();
	});

	// GOOGLE MAP EMBEDS
	$('.locationMap').click(function () {
        $('iframe', this).css("pointer-events", "auto");
    });

    $( ".locationMap" ).mouseleave(function() {
    	$('iframe', this).css("pointer-events", "none");
    });

    $('.navLocations').click(function() {
	    if (overallWidth < 768) {
	    	$('html, body').animate({
		        scrollTop: ($("#locations").offset().top)
		    }, 1100, 'easeOutQuint');
	    } else {
	    	$('html, body').animate({
		        scrollTop: ($("#locations").offset().top - headerHeight)
		    }, 1100, 'easeOutQuint');
	    }
 	});

 	// REORDER HOME PAGE ITEMS ON MOBILE - BECAUSE THIS IS AN AWESOME THING TO DO
 	function reorderHp() {
 		if (overallWidth < 768) {
 			$('.iBox1').insertAfter('.iBox2');
 		} else {
 			$('.iBox1').insertBefore('.iBox2');
 		}
 	}
 	reorderHp();

 	//TEAM FORM TOGGLE
 	$('.teamFormToggle').click(function() {
 		if ($('.joinTeamForm').hasClass('on')) {
 			$('.joinTeamForm').slideUp('fast');
 			$('.joinTeamForm').removeClass('on');
 		} else {
 			$('.joinTeamForm').slideDown('slow');
 			$('.joinTeamForm').addClass('on');
 		}
 	});

 	//MENU TOGGLE
 	$('.menuToggle').click(function() {
 		if ($(this).hasClass('on')) {
 			$('.mainNav').removeClass('on');
 			$(this).removeClass('on');
 		} else {
 			$('.mainNav').addClass('on');
 			$(this).addClass('on');
 		}
 	});

 	//MENU ACCORDION
 	$('.menuAccordionHeader').click(function() {
 		if (overallWidth < 768) {
 			if ($(this).hasClass('on')) {
 				$(this).removeClass('on');
 				$(this).next('.menuAccordionContent').slideUp();
 				$(this).removeClass('on');
 			} else {
 				$('.menuAccordionContent').slideUp();
 				$('.menuAccordionHeader').removeClass('on');
 				$(this).next('.menuAccordionContent').slideDown();
 				$(this).addClass('on');
 			}
 		}
 	});

 	$(window).resize(function() {
 		overallWidth = $(window).width();
 		if (overallWidth < 768) {
 			$('.aboutBox9').insertAfter('.aboutBox10');
 		} else {
 			$('.aboutBox9').insertBefore('.aboutBox10');
 			$('.menuAccordionContent').show();
 		}
 	});

 	if (overallWidth < 768) {
		$('.aboutBox9').insertAfter('.aboutBox10');
	} else {
		$('.aboutBox9').insertBefore('.aboutBox10');
	}

	// NEWSLETTER POPUP
	function showNewsletterPopup() {
		$('.newsletterPopup').fadeIn();
	}

	$('.newsletterPopupClose').click(function() {
		$('.newsletterPopup').fadeOut('fast');
	});

	$('.iBoxPopup').click(function(e) {
		showNewsletterPopup();
		e.preventDefault();
	});

	ouibounce($('.newsletterPopup')[0]);

});

$(window).load(function() {


});

})(jQuery);