require 'middleman-core/cli'
require 'net/http'
require 'json'

module MiddlemanKenticoDelivery
  class KenticoDelivery < Thor::Group
  	include Thor::Actions

  	#namespace :kenticoDelivery
  	#desc 'kenticoDelivery', 'Import data from Kentico Cloud'

    PROJECT_ID = 'a318a518-13e1-4d16-a56a-baf74865f562'
    PREVIEW_API_KEY = ''

    PREVIEW_BASE_URL = 'https://preview-deliver.kenticocloud.com/'
    LIVE_BASE_URL = 'https://deliver.kenticocloud.com/'

    MIDDLEMAN_LOCAL_DATA_FOLDER = 'data'

    def deliver
      ::Middleman::Application.new

      Dir.mkdir('data') unless File.exists?('data')

      FileUtils.rm_rf(Dir.glob('data/kentico_*'))

      File.open("data/kentico_main_navigation.yaml", "w") do |f|
      	f.write(getJsonContent('items?system.type=ordered_items_module&system.codename=main_navigation'))
    	end

      File.open("data/kentico_home.yaml", "w") do |f|
      	f.write(getJsonContent('items?system.type=home'))
    	end
    	
    	File.open("data/kentico_about.yaml", "w") do |f|
    		f.write(getJsonContent('items?system.type=about_us'))
  		end

			File.open("data/kentico_join_team.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=join_team'))
			end
=begin
			File.open("data/kentico_blog_posts.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=blog_post'))
			end

			File.open("data/kentico_news.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=news'))
			end
=end

			File.open("data/kentico_locations_page.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=locations&depth=2'))
			end

      File.open("data/kentico_menu.yaml", "w") do |f| 
      	f.write(getJsonContent('items?system.type=menu'))
  	  end

  	  File.open("data/kentico_menu_signature_creations.yaml", "w") do |f|
  	  	f.write(getJsonContent('items?system.type=ordered_items_module&system.codename=signature_creations'))
	  	end

			File.open("data/kentico_menu_create_your_own.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=create_your_own&system.codename=create_your_own'))
			end

			File.open("data/kentico_menu_cyo_bases.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=ordered_items_module&system.codename=base_it'))
			end	

			File.open("data/kentico_menu_cyo_build.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=ordered_items_module&system.codename=build_it'))
			end	

			File.open("data/kentico_menu_cyo_toppings.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=ordered_items_module&system.codename=top_it'))
			end	

			File.open("data/kentico_menu_cyo_toppings_cheeses.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=ordered_items_module&system.codename=top_it___cheeses'))
			end	

			File.open("data/kentico_menu_cyo_toppings_proteins.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=ordered_items_module&system.codename=top_it___proteins'))
			end	

			File.open("data/kentico_menu_cyo_toppings_seasonal.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=ordered_items_module&system.codename=top_it___seasonal_toppings'))
			end	

			File.open("data/kentico_menu_cyo_dressings.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=ordered_items_module&system.codename=dress_it'))
			end	

			File.open("data/kentico_menu_seasonal_selections.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=ordered_items_module&system.codename=seasonal_selections'))
			end	

			File.open("data/kentico_menu_soups.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=ordered_items_module&system.codename=soups'))
			end	

			File.open("data/kentico_menu_drinks.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=ordered_items_module&system.codename=drinks'))
			end	

			File.open("data/kentico_menu_juices.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=ordered_items_module&system.codename=juices'))
			end	

			File.open("data/kentico_menu_downloadable_pdfs.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=ordered_items_module&system.codename=downloadable_pdf_menus_'))
			end	

			File.open("data/kentico_locations.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=ordered_items_module&system.codename=location'))
			end

			File.open("data/kentico_landing_vday.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=landing_page&system.codename=valentine_landing_page'))
			end

			File.open("data/kentico_landing_hhh.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=landing_page&system.codename=healthy_happy_hour'))
			end

			File.open("data/kentico_landing_mothersday.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=landing_page&system.codename=mother_s_day_landing'))
			end

			File.open("data/kentico_landing_fiesta.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=landing_page&system.codename=fiesta_landing_page'))
			end

			File.open("data/kentico_landing_kids.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=landing_page&system.codename=kids_landing'))
			end

			File.open("data/kentico_landing_seasonal.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=landing_page&system.codename=seasonal_landing_page'))
			end

			File.open("data/kentico_greenroom_landing.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=landing_page&system.codename=greenroom_landing'))
			end		

			File.open("data/kentico_catering_7bf2e4a.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=landing_page&system.codename=catering_7bf2e4a'))
			end

			File.open("data/kentico_fresh_to_desk_landing_page.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=landing_page&system.codename=fresh_to_desk_landing_page'))
			end		

			File.open("data/kentico_strivefor5.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=landing_page&system.codename=strivefor5_landing_page'))
			end	

			File.open("data/kentico_dressings_landing_page.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=landing_page&system.codename=dressings_landing_page'))
			end	
        
            File.open("data/kentico_wifi_landing_page.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=landing_page&system.codename=wifi_landing_page'))
			end	
        
            File.open("data/kentico_popups_landing_page.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=landing_page&system.codename=popups_landing_page'))
			end	
        
            File.open("data/kentico_safetymeasures_landing_page.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=landing_page&system.codename=safetymeasures_landing_page'))
			end	
        
            File.open("data/kentico_birthday_landing_page.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=landing_page&system.codename=birthday_landing_page'))
			end	
        
            File.open("data/kentico_bday_landing_page.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=landing_page&system.codename=bday_landing_page'))
			end	
        
            File.open("data/kentico_platingchange_landing_page.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=landing_page&system.codename=platingchange_landing_page'))
			end	
        
            File.open("data/kentico_super_landing_page.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=landing_page&system.codename=super_landing_page'))
			end	

            File.open("data/kentico_lifestyle_landing_page.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=landing_page&system.codename=lifestyle_landing_page'))
			end	

			File.open("data/kentico_mesa_landing_page.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=landing_page&system.codename=mesa_landing_page'))
			end	

			File.open("data/kentico_fb_landing_page.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=landing_page&system.codename=fb_landing_page'))
			end

			File.open("data/kentico_fb2_landing_page.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=landing_page&system.codename=fb2_landing_page'))
			end	

			File.open("data/kentico_cola_landing_page.yaml", "w") do |f|
				f.write(getJsonContent('items?system.type=landing_page&system.codename=cola_landing_page'))
			end	
		end

	Middleman::Cli::Base.register(self, 'deliver', '', 'Importa data from Kentico Cloud')

	no_commands do
	  def getJsonContent(urlAppend)
	    url = getUrlWithProjectId + urlAppend
	    uri = URI(url)
		response = Net::HTTP.get(uri)
		return JSON.parse(response).to_yaml
	  end

	  def isPreview
		return PREVIEW_API_KEY != ''
	  end

	  def getBaseUrl() 
		if isPreview
	      return PREVIEW_BASE_URL
		else
		  return LIVE_BASE_URL
		end
	  end

	  def getUrlWithProjectId
		return getBaseUrl() + PROJECT_ID + '/'
	  end

	  def getHeaders
	  	if isPreview
	   	  return "Authorization" => "Bearer " + PREVIEW_API_KEY
	  	end
	  end


	end
  end
end

#function getJsonContent(urlAppend, successCallback, errorCallback) {
#	let url = getUrlWithProjectId() + urlAppend;
#	const headers = getHeaders();

#	$.get(url, successCallback );
#}