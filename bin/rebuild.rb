require 'net/http'
require 'uri'
require 'json'

uri = URI.parse("https://bitbucket.org/site/oauth2/access_token")
request = Net::HTTP::Post.new(uri)

# OAuth for bluekeylabs@gmail.com within Atlassian
# Key:  TvdwSmnBcKvSNDdgTQ
# Secret:  RSbFnQCHEBTHrzB6C3nz7xfQGUBbPzKu
request.basic_auth("TvdwSmnBcKvSNDdgTQ", "RSbFnQCHEBTHrzB6C3nz7xfQGUBbPzKu")
request.set_form_data(
  "grant_type" => "client_credentials",
)

req_options = {
  use_ssl: uri.scheme == "https",
}

response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
  http.request(request)
end

if response.code == "200"
  result = JSON.parse(response.body)
  #puts result["access_token"]

  # post to Heroku
  uri = URI.parse("https://api.heroku.com/apps/cryptic-temple-55866/builds")
  request = Net::HTTP::Post.new(uri)
  request.content_type = "application/json"
  request["Accept"] = "application/vnd.heroku+json; version=3"

  # API Key for bluekeylabs@gmail.com within Heroku
  # ae67d5e4-1e48-4558-b5c3-44c997404071
  request["Authorization"] = "Bearer ae67d5e4-1e48-4558-b5c3-44c997404071"

  url = "https://bitbucket.org/bluekeymadscientists/verde-cloud-middleman/get/HEAD.tar.gz?access_token=" + result["access_token"]

  request.body = JSON.dump({
    "source_blob" => {
      "url" => url
    }
  })

  req_options = {
    use_ssl: uri.scheme == "https",
  }

  response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
    http.request(request)

    # need to evaluate response and report accordingly
    puts "Rebuild command sent."
  end
  #

else
  puts "Failed to obtain access token."
end
